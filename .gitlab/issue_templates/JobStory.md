# Situation
## Habits / State

# Motivation
## Attraction of idea

## Problem with current

# Expected Outcome
## Anxieties of new

<Direction> <metric> <object of control> <clarifiers> <examples>