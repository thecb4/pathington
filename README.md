# Pathington

## Description
Pathington is a derivative of [Path.swift](https://github.com/mxcl/Path.swift). Retaining most of the features, Pathington provides a *fatalError* instead of an *assertion* to ensure failure in a production setting.

---

## Installation

### Swift Package Manager
```swift

```

---

## Usage


---

## Support
Submit questions/comments/concerns via issues

---

## Roadmap

---

## Contributing
Pull Requests are welcome.

---

## Acknowledgements

---

## License
This project uses an [MIT License](./LICENSE.md)

---

## Project Status
This is a nacent project. It will evolve over time.
