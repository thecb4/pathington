import XCTest

@testable import PathingtonWithFlag

final class PathingtonTests: XCTestCase {

  override static func setUp() {
    setenv("TEST_FATALERROR", "1", 1)
  }

  func testRootDirectory() {
    let path: DynamicPath = "/"
    XCTAssertEqual(CommonPaths.root, path)
  }

  func testCurrentWorkingDirectory() {
    XCTAssertEqual(CommonPaths.cwd.string, FileManager.default.currentDirectoryPath)
  }

  func testCurrentUserHomeDirectory() {
    XCTAssertEqual(CommonPaths.home.string, FileManager.default.homeDirectoryForCurrentUser.path)
  }

  func testJoiningPaths() {
    XCTAssertEqual((CommonPaths.root / "bar").string, "/bar")
  }

  func xtestExpressibleByStringLiteral() {
    let path: Path = "/foo/bar"
    XCTAssertEqual(path.string, "/foo/bar")
  }

  func testDynamicPath() {
    let path: DynamicPath = CommonPaths.root.foo.bar
    XCTAssertEqual(path.string, "/foo/bar")
  }

  func testURL() {
    let path: Path = "/some/folder"
    XCTAssertEqual(path.url.description, "file:///some/folder")
  }

  func testFatalError() {
    expectFatalError(expectedMessage: "Bad Path:  ") {
      let _: Path = " "
    }
  }

  func testPathURL() {
    let url = URL(fileURLWithPath: "Tests/Fixtures")
    if let path = Path(url) {
      XCTAssertEqual(path, CommonPaths.cwd / "Tests/Fixtures")
    } else {
      XCTFail()
    }
  }

  func testPathTilde() {
    let path = Path("~/work")
    XCTAssertEqual(path, CommonPaths.home / "work")
  }

  func testPathDot() {
    let path = Path(".")
    XCTAssertEqual(path, Path(CommonPaths.cwd))
  }

  func testPathTwoDots() {
    let path = Path("../")
    XCTAssertEqual(path, Path(CommonPaths.cwd.parent))
  }

  func testCleanedPath() {

    XCTAssertEqual(Path("///"), Path(CommonPaths.root))

    XCTAssertEqual(
      Path("////////////some/ridiculous/folder"), Path(CommonPaths.root / "some/ridiculous/folder"))

    XCTAssertEqual(CommonPaths.root / "foo///bar////", Path(CommonPaths.root / "foo/bar"))

    XCTAssertEqual(CommonPaths.root.foo.bar.join(".."), Path(CommonPaths.root.foo))

    XCTAssertEqual(CommonPaths.root.foo.bar.join("."), Path(CommonPaths.root.foo.bar))

    XCTAssertEqual(CommonPaths.root.foo.bar.join("../baz"), Path(CommonPaths.root.foo.baz))
  }

  func testRootWithTilde() {
    XCTAssertEqual(Path("/~////"), CommonPaths.root / "~")
  }

  func testPathMultipleTwoDots() {
    XCTAssertEqual(CommonPaths.root / "a/foo" / "../bar", CommonPaths.root / "a/bar")
    XCTAssertEqual(CommonPaths.root / "a/foo" / "/../bar", CommonPaths.root / "a/bar")
    XCTAssertEqual(CommonPaths.root / "a/foo" / "../../bar", CommonPaths.root / "bar")
    XCTAssertEqual(CommonPaths.root / "a/foo" / "../../../bar", CommonPaths.root / "bar")
  }

}

// https://medium.com/@marcosantadev/how-to-test-fatalerror-in-swift-e1be9ff11a29
extension XCTestCase {
  func expectFatalError(expectedMessage: String, testcase: @escaping () -> Void) {
    let expectation = self.expectation(description: "expectingFatalError")
    var assertionMessage: String? = nil

    FatalErrorUtil.replaceFatalError { message, _, _ in
      assertionMessage = message
      expectation.fulfill()
      self.unreachable()
    }

    DispatchQueue.global(qos: .userInitiated).async(execute: testcase)

    waitForExpectations(timeout: 2) { _ in
      XCTAssertEqual(assertionMessage, expectedMessage)
      FatalErrorUtil.restoreFatalError()
    }
  }

  private func unreachable() -> Never {
    repeat {
      RunLoop.current.run()
    } while true
  }
}
