import Foundation

public protocol Pathable: Hashable, Comparable {
  var string: String { get }
}

extension Pathable {
  public var url: URL {
    return URL(fileURLWithPath: string)
  }

  /**
  Returns the parent directory for this path.
  Path is not aware of the nature of the underlying file, but this is
  irrlevant since the operation is the same irrespective of this fact.
  - Note: always returns a valid path, `Path.root.parent` *is* `Path.root`.
  */
  public var parent: Path {
    let index = string.lastIndex(of: "/")!
    guard index != string.indices.startIndex else { return Path("/") }
    let substr = string[string.indices.startIndex..<index]
    return Path(String(substr))
  }

  /**
     Returns the filename extension of this path.
     - Remark: If there is no extension returns "".
     - Remark: If the filename ends with any number of ".", returns "".
     - Note: We special case eg. `foo.tar.gz`—there are a limited number of these specializations, feel free to PR more.
     */
  @inlinable
  var `extension`: String {
    //FIXME efficiency
    switch true {
      case string.hasSuffix(".tar.gz"):
        return "tar.gz"
      case string.hasSuffix(".tar.bz"):
        return "tar.bz"
      case string.hasSuffix(".tar.bz2"):
        return "tar.bz2"
      case string.hasSuffix(".tar.xz"):
        return "tar.xz"
      default:
        let slash = string.lastIndex(of: "/")!
        if let dot = string.lastIndex(of: "."), slash < dot {
          let foo = string.index(after: dot)
          return String(string[foo...])
        } else {
          return ""
        }
    }
  }

  /**
     Returns a string representing the relative path to `base`.
     - Note: If `base` is not a logical prefix for `self` your result will be prefixed some number of `../` components.
     - Parameter base: The base to which we calculate the relative path.
     - ToDo: Another variant that returns `nil` if result would start with `..`
     */
  func relative<P: Pathable>(to base: P) -> String {
    // Split the two paths into their components.
    // FIXME: The is needs to be optimized to avoid unncessary copying.
    let pathComps = (string as NSString).pathComponents
    let baseComps = (base.string as NSString).pathComponents

    // It's common for the base to be an ancestor, so try that first.
    if pathComps.starts(with: baseComps) {
      // Special case, which is a plain path without `..` components.  It
      // might be an empty path (when self and the base are equal).
      let relComps = pathComps.dropFirst(baseComps.count)
      return relComps.joined(separator: "/")
    } else {
      // General case, in which we might well need `..` components to go
      // "up" before we can go "down" the directory tree.
      var newPathComps = ArraySlice(pathComps)
      var newBaseComps = ArraySlice(baseComps)
      while newPathComps.prefix(1) == newBaseComps.prefix(1) {
        // First component matches, so drop it.
        newPathComps = newPathComps.dropFirst()
        newBaseComps = newBaseComps.dropFirst()
      }
      // Now construct a path consisting of as many `..`s as are in the
      // `newBaseComps` followed by what remains in `newPathComps`.
      var relComps = Array(repeating: "..", count: newBaseComps.count)
      relComps.append(contentsOf: newPathComps)
      return relComps.joined(separator: "/")
    }
  }

  /**
     The basename for the provided file, optionally dropping the file extension.
         Path.root.join("foo.swift").basename()  // => "foo.swift"
         Path.root.join("foo.swift").basename(dropExtension: true)  // => "foo"
     - Returns: A string that is the filename’s basename.
     - Parameter dropExtension: If `true` returns the basename without its file extension.
     */
  func basename(dropExtension: Bool = false) -> String {
    var lastPathComponent: Substring {
      let slash = string.lastIndex(of: "/")!
      let index = string.index(after: slash)
      return string[index...]
    }
    var go: Substring {
      if !dropExtension {
        return lastPathComponent
      } else {
        let ext = self.extension
        if !ext.isEmpty {
          return lastPathComponent.dropLast(ext.count + 1)
        } else {
          return lastPathComponent
        }
      }
    }
    return String(go)
  }

  public var components: [String] {
    string.components(separatedBy: "/")
  }

  public func join(_ value: String) -> Path {
    Path(append(value, to: string))
  }

  func append<S>(_ value: S, to prefix: String) -> String where S: StringProtocol {

    let components = value.components(separatedBy: "/").filter { $0 != "" }

    var pathString = prefix

    for component in components {

      if component.contains("/") { fatalError("Bad Path Component: \(component)") }

      switch component {
        case "..":
          let start = pathString.indices.startIndex
          let index = pathString.lastIndex(of: "/")!
          if start == index {
            pathString = "/"
          } else {
            pathString = String(pathString[start..<index])
          }
        case ".":
          break
        default:
          if pathString == "/" {
            pathString = "/\(component)"
          } else {
            pathString = "\(pathString)/\(component)"
          }
      }

    }

    return pathString
  }

  static func clean(_ string: String) -> String {

    let components = string.components(separatedBy: "/")

    let removedExtraForwardSlashes = components.filter { $0 != "/ " }

    let cleaned = "/" + (removedExtraForwardSlashes.filter { $0 != "" }).joined(separator: "/")

    return cleaned
  }

  public static func < (lhs: Self, rhs: Self) -> Bool {
    lhs.string.compare(rhs.string, options: .caseInsensitive, locale: .current) == .orderedAscending
  }

  public static func / (lhs: Self, rhs: Self) -> Path {
    return lhs / rhs.string
  }

  public static func / (lhs: Self, rhs: String) -> Path {
    if rhs == "" {
      return Path(lhs)
    } else {
      return lhs.join(rhs)
    }
  }
}
