/// A path that supports arbituary dot notation, eg. `Path.root.usr.bin`
@dynamicMemberLookup
public struct DynamicPath: Pathable {
  /// The normalized string representation of the underlying filesystem path
  public let string: String

  init(_ string: String) {
    assert(string.hasPrefix("/"))
    self.string = string
  }

  /// Converts a `Path` to a `DynamicPath`
  public init<P: Pathable>(_ path: P) {
    string = path.string
  }

  /// :nodoc:
  public subscript(dynamicMember addendum: String) -> DynamicPath {
    //NOTE it’s possible for the string to be anything if we are invoked via
    // explicit subscript thus we use our fully sanitized `join` function
    return DynamicPath(append(addendum, to: string))
  }
}

extension DynamicPath: ExpressibleByStringLiteral {
  public init(stringLiteral value: String) {
    self = .init(value)
  }
}

extension DynamicPath: CustomStringConvertible {
  public var description: String {
    self.string
  }
}

extension DynamicPath: CustomDebugStringConvertible {
  /// Returns eg. `Path(string: "/foo")`
  public var debugDescription: String {
    return "Path(\(string))"
  }
}
