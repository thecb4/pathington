import Foundation

extension Pathable {

  @discardableResult public func touch() throws -> Path {
    if let _ = self.type {
      try FileManager.default.setAttributes([.modificationDate: Date()], ofItemAtPath: string)
    } else {
      guard FileManager.default.createFile(atPath: string, contents: nil) else {
        throw PathError.fileCreationError(path: self.string)
      }
    }

    return Path(self)
  }

  /**
     Deletes the path, recursively if a directory.
     - Note: noop: if the path doesn’t exist
     ∵ *Path.swift* doesn’t error if desired end result preexists.
     - Note: On UNIX will this function will succeed if the parent directory is writable and the current user has permission.
     - Note: This function will fail if the file or directory is “locked”
     - Note: If entry is a symlink, deletes the symlink.
     - SeeAlso: `lock()`
    */
  @inlinable
  public func delete() throws {
    if type != nil {
      try FileManager.default.removeItem(at: url)
    }
  }

  @discardableResult public func rename(to name: String) throws -> Path {
    let newPath = self.parent / name
    try FileManager.default.moveItem(atPath: self.string, toPath: newPath.string)
    return newPath
  }

  /**
     Creates the directory at this path.
     - Parameter options: Specify `mkdir(.p)` to create intermediary directories.
     - Note: We do not error if the directory already exists (even without `.p`)
       because *Path.swift* noops if the desired end result preexists.
     - Returns: A copy of `self` to allow chaining.
     */
  @discardableResult public func mkdir(_ options: MakeDirectoryOptions = []) throws -> Path {
    try FileManager.default.createDirectory(
      at: self.url, withIntermediateDirectories: options.contains(.p), attributes: nil)
    return Path(self)
  }

  @discardableResult
  public func copy<P: Pathable>(to: P, overwrite: Bool = false) throws -> Path {
    if overwrite, let tokind = to.type, tokind != .directory, self.type != .directory {
      try FileManager.default.removeItem(at: to.url)
    }
    try FileManager.default.copyItem(atPath: self.string, toPath: to.string)
    return Path(to)
  }

  @discardableResult
  public func copy<P: Pathable>(to: [P], overwrite: Bool = false) throws -> [Path] {
    for item in to {
      try copy(to: item, overwrite: overwrite)
    }
    return to.map { Path($0) }
  }

  @discardableResult
  public func copy<P: Pathable>(into: P, overwrite: Bool = false) throws -> Path {

    let newPath = into / basename()

    if !overwrite, newPath.type != nil {
      throw PathError.fileExistsError(path: newPath.string)
    }

    if into.type == nil {
      try into.mkdir([.p])
    }

    if overwrite, let kind = newPath.type, kind != .directory {
      try FileManager.default.removeItem(at: newPath.url)
    }

    try FileManager.default.copyItem(at: self.url, to: newPath.url)

    return newPath
  }

  @discardableResult
  public func copy<P: Pathable>(into: [P], overwrite: Bool = false) throws -> [Path] {
    for item in into {
      try copy(to: item, overwrite: overwrite)
    }
    return into.map { Path($0) }
  }

}

public struct MakeDirectoryOptions: OptionSet {
  public let rawValue: Int

  public init(rawValue: Int) {
    self.rawValue = rawValue
  }

  static let p = MakeDirectoryOptions(rawValue: 1 << 0)

  static let all: MakeDirectoryOptions = [.p]
}
