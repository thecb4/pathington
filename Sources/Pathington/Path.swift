import Foundation

public struct Path: Pathable {
  public let string: String

  // fatal error when path is bad https://blog.krzyzanowskim.com/2015/03/09/swift-asserts-the-missing-manual/
  public init(_ string: String) {

    guard let first = string.first else { fatalError("Bad Path: \(string)") }

    let prefix = "\(first)"

    let prefixes = ["/", "~", ".", ".."]

    guard let _ = (prefixes.filter { $0 == prefix }).first else {
      fatalError("Bad Path: \(string)")
    }

    switch prefix {
      case "/":
        self.string = Path.clean(string)
      case "~":
        if String(string.prefix(2)) == "~/" {
          self = Path(CommonPaths.home / String(string.dropFirst(2)))
        } else {
          fatalError("Bad Path: \(string)")
        }
      case ".":
        if string.count == 1 {
          self = Path(CommonPaths.cwd)
        } else if String(string.prefix(3)) == "../" {
          self = CommonPaths.cwd.parent / String(string.dropFirst(3))
        } else {
          fatalError("Bad Path: \(string)")
        }
      default:
        fatalError("Bad Path: \(string)")
    }

  }

}

extension Path {

  public init<P: Pathable>(_ path: P) {
    self.string = path.string
  }

  /**
     Creates a new absolute, standardized path from the provided file-scheme URL.
     - Note: If the URL is not a file URL, returns `nil`.
    */
  public init?(_ url: URL) {
    guard url.scheme == "file" else { return nil }
    self.init(url.path)
    //NOTE: URL cannot be a file-reference url, unlike NSURL, so this always works
  }

}

extension Path: ExpressibleByStringLiteral {
  public init(stringLiteral value: String) {
    self = Path(value)
  }
}

extension Path: CustomStringConvertible {
  public var description: String {
    self.string
  }
}

extension Path: CustomDebugStringConvertible {
  /// Returns eg. `Path(string: "/foo")`
  public var debugDescription: String {
    return "Path(\(string))"
  }
}

extension Path {
  public enum Kind: CaseIterable {
    case file
    case symlink
    case directory
  }
}
