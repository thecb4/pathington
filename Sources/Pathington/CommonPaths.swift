import Foundation

enum CommonPaths {
  static var cwd: DynamicPath {
    .init(FileManager.default.currentDirectoryPath)
  }

  static var home: DynamicPath {
    .init(FileManager.default.homeDirectoryForCurrentUser.path)
  }

  static var root: DynamicPath {
    .init("/")
  }

  public static func source(for filePath: String = #filePath) -> (
    file: DynamicPath, directory: DynamicPath
  ) {
    let file = DynamicPath(filePath)
    return (file: file, directory: .init(file.parent))
  }

  /// Helper to allow search path and domain mask to be passed in.
  private static func path(for searchPath: FileManager.SearchPathDirectory) -> DynamicPath {
    guard
      let pathString = FileManager.default.urls(for: searchPath, in: .userDomainMask).first?.path
    else {
      fatalError("Bad search path: \(searchPath)")
    }
    return .init(pathString)
  }

  /**
     The root for user documents.
     - Note: There is no standard location for documents on Linux, thus we return `~/Documents`.
     - Note: You should create a subdirectory before creating any files.
     */
  public static var documents: DynamicPath {
    return path(for: .documentDirectory)
  }

  /**
     The root for cache files.
     - Note: On Linux this is `XDG_CACHE_HOME`.
     - Note: You should create a subdirectory before creating any files.
     */
  public static var caches: DynamicPath {
    return path(for: .cachesDirectory)
  }

  /**
     For data that supports your running application.
     - Note: On Linux is `XDG_DATA_HOME`.
     - Note: You should create a subdirectory before creating any files.
     */
  public static var applicationSupport: DynamicPath {
    return path(for: .applicationSupportDirectory)
  }
}
