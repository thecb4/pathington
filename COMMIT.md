Script Fixes

Fixed:
- Merge request title now shows correct branch name

Changed:
- Feature finish now requires each step to complete before moving on to the next.
