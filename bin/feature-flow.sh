#!/usr/bin/env bash

bin/feature-review.sh
bin/feature-build.sh
bin/feature-test.sh
bin/feature-commit.sh