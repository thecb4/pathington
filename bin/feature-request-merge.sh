#!/usr/bin/env bash

branch=$(git branch --show-current)
description=$(head -1 COMMIT.md)

git push \
-o merge_request.create \
-o merge_request.target=main \
-o merge_request.merge_when_pipeline_succeeds \
-o merge_request.remove_source_branch \
-o merge_request.title="$branch" \
-o merge_request.description="$description" \
origin "$branch"
