#!/usr/bin/env bash

UPDATED_COMMIT=$(git diff --name-only | grep COMMIT.md)

if [ "$UPDATED_COMMIT" = "COMMIT.md" ]; then
	echo "😁 Committing files"
  git add -A && git commit -F COMMIT.md
else
	echo "🙄 Please update COMMIT.md"
fi